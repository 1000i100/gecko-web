import 'package:flutter/material.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:provider/provider.dart';

class CertsCounter extends StatelessWidget {
  const CertsCounter({Key? key, required this.address, this.isSent = false})
      : super(key: key);
  final String address;
  final bool isSent;

  @override
  Widget build(BuildContext context) {
    return Consumer2<PolkadotProvider, PolkadotSubscribBlocksProvider>(
        builder: (context, polka, polkaBlock, _) {
      return Text('(${polka.certsCounterCache[address]![isSent ? 1 : 0]})',
          style: const TextStyle(color: Colors.black));
    });
  }
}
