import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/widgets/balance.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

class HeaderProfileView extends StatelessWidget {
  const HeaderProfileView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: true);

    return Stack(children: <Widget>[
      Container(
          height: 180,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                yellowC,
                Color(0xFFE7811A),
              ],
            ),
          )),
      Padding(
        padding: const EdgeInsets.only(left: 30, right: 40),
        child: Row(children: <Widget>[
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(height: 30),
                Row(children: [
                  InkWell(
                    key: const Key('copyPubkey'),
                    onTap: () {
                      Clipboard.setData(
                          ClipboardData(text: homeProvider.currentAddress));
                      homeProvider.snackCopyKey(
                          context, homeProvider.currentAddress);
                    },
                    child: Text(
                      homeProvider.getShortPubkey(homeProvider.currentAddress),
                      style: TextStyle(
                        color: Colors.grey[900],
                        fontSize: 22,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                ]),
                const SizedBox(height: 25),
                Balance(address: homeProvider.currentAddress, size: 20),
                const SizedBox(height: 10),
                InkWell(
                  onTap: () {
                    homeProvider.showCertsView = true;
                    homeProvider.reload();
                    // if (screenWidth < 1130) {
                    //   Scaffold.of(context).openEndDrawer();
                    // }
                    // context.router.push(CertificationsRoute(
                    //     address: homeProvider.currentAddress,
                    //     username: homeProvider.currentName));
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(homeProvider.currentName,
                          style: TextStyle(
                              color: Colors.grey[900],
                              fontSize: 18,
                              fontWeight: FontWeight.w600)),
                      homeProvider.getCerts(context,
                          homeProvider.currentAddress, 17, Colors.grey[900]!),
                    ],
                  ),
                ),
                const SizedBox(height: 40),
              ]),
          const Spacer(),
          Column(children: <Widget>[
            QrImageWidget(
              data: homeProvider.currentAddress,
              version: QrVersions.auto,
              size: 120,
            ),
          ]),
        ]),
      ),
    ]);
  }
}
