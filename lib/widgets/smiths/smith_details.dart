import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/smiths/certs_widget.dart';
import 'package:gecko_web/widgets/smiths/expiration_widget.dart';
import 'package:gecko_web/widgets/smiths/session_expiration.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

class SmithDetails extends StatelessWidget {
  const SmithDetails({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final smithsP = Provider.of<SmithsProvider>(context, listen: false);
    final polka = Provider.of<PolkadotProvider>(context, listen: false);
    final smith = smithsP.currentSmith;

    return Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Text(
        smith.username!,
        style: const TextStyle(
            fontSize: 20, decoration: TextDecoration.none, color: Colors.black),
      ),
      GestureDetector(
        key: const Key('copyPubkey'),
        onTap: () {
          Clipboard.setData(ClipboardData(text: smith.address));
          snackCopyKey(context, smith.address);
        },
        child: Text(
          getShortPubkey(smith.address),
          style: const TextStyle(
              fontSize: 20,
              decoration: TextDecoration.none,
              color: Colors.black),
        ),
      ),
      GestureDetector(
        key: const Key('copyPubkeyBis'),
        onTap: () {
          Clipboard.setData(ClipboardData(text: smith.address));
          snackCopyKey(context, smith.address);
        },
        child: QrImageWidget(
          data: smith.address,
          version: QrVersions.auto,
          size: 120,
        ),
      ),
      Text(
        smith.idtyIndex.toString(),
        style: const TextStyle(
            fontSize: 16, decoration: TextDecoration.none, color: Colors.black),
      ),
      Text(
        'membercertif'.tr(),
        style: const TextStyle(
            fontSize: 20, decoration: TextDecoration.none, color: Colors.black),
      ),
      CertsWidget(
        getCerts: polka.getCerts(smith.address),
        address: 'address',
        icon: 'assets/medal.png',
      ),
      ExpirationWidget(
          getExpiration: smithsP.getMembershipExpiration(smith), smith: smith),
      Text(
        'smithcertif'.tr(),
        style: const TextStyle(
            fontSize: 20, decoration: TextDecoration.none, color: Colors.black),
      ),
      CertsWidget(
        getCerts: smithsP.getSmithCerts(smith),
        address: 'address',
        icon: 'assets/server.png',
      ),
      ExpirationWidget(
          getExpiration: smithsP.getSmithMembershipExpiration(smith),
          smith: smith),
      SessionExpirationWidget(
          getSessionExpiration: smithsP.getSessionExpiration(smith.idtyIndex),
          smith: smith),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("${"status".tr()}: ", style: mainStyle),
          Text(
            "${smith.status?.name}",
            style: TextStyle(
              fontSize: 20,
              decoration: TextDecoration.none,
              color: onlineColor(smith.status),
            ),
          ),
        ],
      )
    ]);
  }
}
