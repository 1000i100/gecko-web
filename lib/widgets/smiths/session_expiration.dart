import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/polkadot_subscribe_sessions.dart';
import 'package:gecko_web/utils.dart';
import 'package:provider/provider.dart';

class SessionExpirationWidget extends StatelessWidget {
  const SessionExpirationWidget({
    required this.getSessionExpiration,
    required this.smith,
    super.key,
  });

  final SmithData smith;
  final Future<List<int>> getSessionExpiration;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getSessionExpiration,
        builder:
            (BuildContext context, AsyncSnapshot<List<int>> sessionExpiration) {
          if (sessionExpiration.hasError ||
              sessionExpiration.data == null ||
              sessionExpiration.connectionState != ConnectionState.done) {
            return const SizedBox();
          }

          DateTime dateSessionExpire =
              sessionNumberToDate(sessionExpiration.data?[0] ?? 0);
          DateTime dateRotateKey =
              sessionNumberToDate(sessionExpiration.data?[1] ?? 0);
          return Consumer<PolkadotSubscribSessionsProvider>(
              builder: (context, polkaSubSession, _) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                smith.status == SmithStatus.offline
                    ? polkaSubSession.sessionNumber < sessionExpiration.data![0]
                        ? Text(
                            "${"sessionExpire".tr()} ${DateFormat('d MMM yyyy').format(dateSessionExpire)} (session: ${sessionExpiration.data?[0] ?? 0})",
                            style: TextStyle(
                                fontSize: 16,
                                decoration: TextDecoration.none,
                                color: sessionExpirationColor(
                                    polkaSubSession.sessionNumber,
                                    sessionExpiration.data![0])),
                          )
                        : Text(
                            "keysexpired".tr(),
                            style: const TextStyle(
                                fontSize: 16,
                                decoration: TextDecoration.none,
                                color: Colors.red),
                          )
                    : const Text(""),
                const SizedBox(width: 2),
                smith.status == SmithStatus.online
                    ? polkaSubSession.sessionNumber < sessionExpiration.data![1]
                        ? Text(
                            "${"mustRotateKeys".tr()} ${DateFormat('d MMM yyyy').format(dateRotateKey)} (session: ${sessionExpiration.data?[1] ?? 0})",
                            style: TextStyle(
                                fontSize: 16,
                                decoration: TextDecoration.none,
                                color: sessionExpirationColor(
                                    polkaSubSession.sessionNumber,
                                    sessionExpiration.data![1])),
                            //dateRotateKey > (sessionNumberToDate(polkaSubSession.sessionNumber))-(Duration(days: 30)) ? :
                          )
                        : Text(
                            "keysexpired".tr(),
                            style: const TextStyle(
                                fontSize: 16,
                                decoration: TextDecoration.none,
                                color: Colors.red),
                          )
                    : const Text(""),
              ],
            );
          });
        });
  }
}
