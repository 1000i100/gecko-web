import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:provider/provider.dart';

class SmithTile extends StatelessWidget {
  const SmithTile({
    super.key,
    required this.smith,
  });

  final SmithData smith;

  @override
  Widget build(BuildContext context) {
    final smithsP = Provider.of<SmithsProvider>(context, listen: false);

    return InkWell(
      onTap: () {
        smithsP.currentSmith = smith;
        Scaffold.of(context).openEndDrawer();
      },
      child: Card(
        color: yellowC,
        elevation: 3,
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Text(
            smith.username!,
            style: const TextStyle(
                fontSize: 20,
                decoration: TextDecoration.none,
                color: Colors.black),
          ),
        ]),
      ),
    );
  }
}
