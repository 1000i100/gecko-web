import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class CertsWidget extends StatelessWidget {
  const CertsWidget({
    required this.getCerts,
    required this.address,
    required this.icon,
    super.key,
  });

  final String address;
  final Future<List<int>> getCerts;
  final String icon;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getCerts,
        builder: (BuildContext context, AsyncSnapshot<List<int>> certs) {
          if (certs.hasError ||
              certs.data == null ||
              certs.connectionState != ConnectionState.done) {
            return const SizedBox();
          }

          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Image.asset(icon, height: 25),
                  const SizedBox(width: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${"sent".tr()}: ${certs.data?[0].toString() ?? '0'}",
                        style: const TextStyle(
                            fontSize: 16,
                            decoration: TextDecoration.none,
                            color: Colors.black),
                      ),
                      const SizedBox(width: 2),
                      //for issued certifications
                      Text(
                        "${"received".tr()}: ${certs.data?[1].toString() ?? '0'}",
                        style: const TextStyle(
                            fontSize: 16,
                            decoration: TextDecoration.none,
                            color: Colors.black),
                      ),
                    ],
                  ),
                  // const SizedBox(width: 2),
                  // Image.asset('assets/arrow.png', height: 25),
                ],
              ),
            ],
          );
        });
  }
}
