import 'package:flutter/material.dart';
import 'package:gecko_web/widgets/smiths/smith_tile.dart';

class GridWidget extends StatelessWidget {
  const GridWidget({
    required this.nTule,
    required this.smithsTiles,
    super.key,
  });

  final int nTule;
  final List<SmithTile> smithsTiles;

  @override
  Widget build(BuildContext context) {
    return GridView(
        shrinkWrap: true,
        primary: false,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          // childAspectRatio: 3 / 2,
          crossAxisCount: nTule,
          mainAxisExtent: 40,
          crossAxisSpacing: 20, // vertical spacing
          mainAxisSpacing: 20, // horizontal spacing
        ),
        children: smithsTiles);
  }
}
