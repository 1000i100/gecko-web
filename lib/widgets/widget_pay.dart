import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/pay.dart';
import 'package:gecko_web/providers/my_theme.dart';
import 'package:provider/provider.dart';

class PayPopup extends StatelessWidget {
  const PayPopup({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    return Consumer<PayProvider>(builder: (context, pay, _) {
      final theme = Provider.of<MyTheme>(context, listen: true);

      return Container(
        height: 220,
        width: double.infinity,
        decoration: ShapeDecoration(
          color: theme.isDark
              ? const Color.fromARGB(255, 59, 55, 49)
              : const Color(0xffffeed1),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
        ),
        child: Padding(
          padding:
              const EdgeInsets.only(top: 24, bottom: 0, left: 24, right: 24),
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'executeATransfer'.tr(),
                  style: TextStyle(
                      color: theme.isDark ? Colors.grey[100] : Colors.grey[900],
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 10),
                Text(
                  'amount'.tr(),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color:
                          theme.isDark ? Colors.grey[300] : Colors.grey[600]),
                ),
                const SizedBox(height: 10),
                TextField(
                  controller: homeProvider.payAmount,
                  autofocus: false,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  onChanged: (_) => pay.reload(),
                  inputFormatters: <TextInputFormatter>[
                    // FilteringTextInputFormatter.digitsOnly,
                    FilteringTextInputFormatter.deny(',',
                        replacementString: '.'),
                    FilteringTextInputFormatter.allow(
                        RegExp(r'(^\d+\.?\d{0,2})')),
                  ],
                  // onChanged: (v) => _searchProvider.rebuildWidget(),
                  decoration: InputDecoration(
                    hintText: '0.00',
                    hintStyle: TextStyle(
                        color:
                            theme.isDark ? Colors.grey[100] : Colors.grey[600]),
                    suffix: Text(
                      currencyName,
                      style: TextStyle(
                          color: theme.isDark
                              ? Colors.grey[50]
                              : Colors.grey[600]),
                    ),
                    filled: true,
                    fillColor: Colors.transparent,
                    // border: OutlineInputBorder(
                    //     borderSide:
                    //         BorderSide(color: Colors.grey[500], width: 2),
                    //     borderRadius: BorderRadius.circular(8)),

                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: theme.isDark
                              ? Colors.grey[100]!
                              : Colors.grey[500]!,
                          width: 2),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: theme.isDark
                              ? Colors.grey[500]!
                              : Colors.grey[500]!,
                          width: 2),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    contentPadding: const EdgeInsets.all(15),
                  ),
                  style: TextStyle(
                    fontSize: 20,
                    color: theme.isDark ? Colors.grey[100] : Colors.black,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 20),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  SizedBox(
                    width: 200,
                    height: 40,
                    child: Consumer<HomeProvider>(builder: (context, homeP, _) {
                      return ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          foregroundColor: Colors.white,
                          disabledForegroundColor:
                              Colors.black.withOpacity(0.38),
                          disabledBackgroundColor:
                              Colors.black.withOpacity(0.12),
                          backgroundColor: orangeC,
                          elevation: 4,
                        ),
                        onPressed: homeP.payAmount.text != ''
                            ? () {
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(builder: (context) {
                                //     return const SearchResultScreen();
                                //   }),
                                // );
                              }
                            : null,
                        child: Text(
                          'executeTheTransfer'.tr(),
                          style: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w500),
                        ),
                      );
                    }),
                  ),
                ]),
                // IconButton(
                //   iconSize: 40,
                //   icon: const Icon(Icons.cancel_outlined),
                //   onPressed: () {
                //     Navigator.pop(context);
                //   },
                // ),
                const SizedBox(height: 20),
              ]),
        ),
      );
    });
  }
}
