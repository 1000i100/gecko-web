// ignore_for_file: avoid_web_libraries_in_flutter

import 'package:flutter/material.dart';
import 'dart:js' as js;

class PolkadotSubscribBlocksProvider with ChangeNotifier {
  int blockNumber = 0;

  Future subscribeNewBlocs() async {
    var callbackJS = js.allowInterop(newBlocsCallback);
    js.context.callMethod('subscribeNewBlocsJS', [callbackJS]);
  }

  void newBlocsCallback(js.JsObject ev) {
    blockNumber = int.parse(ev['number'].toString());
    //simuler une date ultérieure
    //blockNumber = 5500000;
    notifyListeners();
  }
}
