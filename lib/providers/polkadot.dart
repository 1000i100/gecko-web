// ignore_for_file: avoid_web_libraries_in_flutter

import 'dart:convert';
import 'dart:js_util';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/indexer.dart';
import 'package:gecko_web/providers/polkadot_lib.dart';

import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/queries.dart';
import 'package:gecko_web/providers/polkadot_subscribe_sessions.dart';
import 'package:provider/provider.dart';

class PolkadotProvider with ChangeNotifier {
  bool nodeConnected = false;
  int nbrIdentity = 0;
  Map<String, List<int>> certsCounterCache = {};
  final indexer = Indexer();

  Future<Map<String, double>> getBalance(String address) async {
    if (!nodeConnected) {
      return {
        'transferableBalance': 0,
        'free': 0,
        'unclaimedUds': 0,
        'reserved': 0,
      };
    }

    // Get onchain storage values
    // In Gecko web, this is a bit tricky cause promiseToFuture convert, and dynamic JS type, making null values difficult to catch
    final Map balanceGlobal =
        json.decode((await promiseToFuture(getBalanceJS(address))).toString());
    final int idtyIndex = await getIdentityIndex(address);
    final Map? idtyData = idtyIndex == 0
        ? null
        : json.decode(
            (await promiseToFuture(getIdentityDataJS(idtyIndex))).toString());
    final int currentUdIndex =
        int.parse((await promiseToFuture(getCurrentUdIndexJS())).toString());
    final List pastReevals = (await promiseToFuture(getPastReevalsJS()));

    // Compute amount of claimable UDs
    final int unclaimedUds = _computeUnclaimUds(currentUdIndex,
        idtyData?['data']?['firstEligibleUd'] ?? 0, pastReevals);

    // Calculate transferable and potential balance
    final int transferableBalance = (balanceGlobal['free'] + unclaimedUds);

    Map<String, double> finalBalances = {
      'transferableBalance': transferableBalance / 100,
      'free': balanceGlobal['free'] / 100,
      'unclaimedUds': unclaimedUds / 100,
      'reserved': balanceGlobal['reserved'] / 100,
    };

    return finalBalances;
  }

  int _computeUnclaimUds(
      int currentUdIndex, int firstEligibleUd, List pastReevals) {
    int totalAmount = 0;

    if (firstEligibleUd == 0) return 0;

    for (final List reval in pastReevals.reversed) {
      final int revalNbr = reval[0];
      final int revalValue = reval[1];

      // Loop each UDs revaluations and sum unclaimed balance
      if (revalNbr <= firstEligibleUd) {
        final count = currentUdIndex - firstEligibleUd;
        totalAmount += count * revalValue;
        break;
      } else {
        final count = currentUdIndex - revalNbr;
        totalAmount += count * revalValue;
        currentUdIndex = revalNbr;
      }
    }

    return totalAmount;
  }

  Future<int> getIdentitiesNumber() async {
    final promise = getIdentitiesNumberJS();
    nbrIdentity = int.parse(await promiseToFuture(promise));
    // log.d(nbrIdentity);
    notifyListeners();
    return nbrIdentity;
  }

  Future<List<int>> getCerts(String address) async {
    if (nodeConnected) {
      final promise = getCertsJS(address);
      final certsJS = (await promiseToFuture(promise));
      final certs = [
        int.parse(certsJS[0].toString()),
        int.parse(certsJS[1].toString())
      ];

      certsCounterCache.update(address, (value) => certs,
          ifAbsent: () => certs);
      return certs;
    } else {
      return [];
    }
  }

  Future<String> connectNode(BuildContext context) async {
    final polkaSubBlock =
        Provider.of<PolkadotSubscribBlocksProvider>(context, listen: false);
    final polkaSubSession =
        Provider.of<PolkadotSubscribSessionsProvider>(context, listen: false);
    final promise = connectNodeJS(duniterEndpoint);
    final res = (await promiseToFuture(promise)).toString();
    if (res != 'null') {
      polkaSubBlock.subscribeNewBlocs();
      polkaSubSession.subscribeNewSessions();
      await getIdentitiesNumber();
      await getBlockStart();
      nodeConnected = true;
      notifyListeners();
    }
    return res;
  }

  Future<DateTime> getBlockStart() async {
    final result = await indexer.execQuery(getBlockchainStartQ, {});
    if (!result.hasException) {
      startBlockchainTime =
          DateTime.parse(result.data!['block'][0]['created_at']);
      return startBlockchainTime;
    }
    return DateTime(0, 0, 0, 0, 0);
  }

  Future<int> getIdentityIndex(String address) async {
    final idtyIndexJS = await promiseToFuture(getIdentityIndexJS(address));
    return int.parse(
        idtyIndexJS.toString() == '' ? '0' : idtyIndexJS.toString());
  }

  void reload() {
    notifyListeners();
  }
}
