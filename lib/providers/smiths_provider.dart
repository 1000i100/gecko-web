// ignore_for_file: avoid_web_libraries_in_flutter

import 'dart:convert';
import 'dart:js_util';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/indexer.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_lib.dart';
import 'package:provider/provider.dart';

class SmithsProvider with ChangeNotifier {
  final indexer = Indexer();
  int currentSession = 0;
  int nbrIdentity = 0;
  int nbrMembers = 0;
  int nbrAutorities = 0;
  Map<String, SmithData> listSmith = {};
  bool isSmithStartupLoaded = false;
  late SmithData currentSmith;

  Future asyncStartup(BuildContext context) async {
    // Connect to duniter node using javascript interface
    // final polkaSubBlock = PolkadotSubscribBlocksProvider();
    final polka = Provider.of<PolkadotProvider>(context, listen: false);

    // subscribreBalance('5CQ8T4qpbYJq7uVsxGPQ5q2df7x3Wa4aRY6HUWMBYjfLZhnn');

    if (!polka.nodeConnected) {
      await polka.connectNode(context);
    }

    await getMembershipCounter();
    await getAuthoritiesCounter();
    //await getCurrentSession();

    // Init hive database
    // We're using HiveStore for persistence
    // await initHiveForFlutter();

    // Get Smiths
    await getSmithsHardcoded();
    //await getSmiths();
    isSmithStartupLoaded = true;
    notifyListeners();
  }

  //Sum of membership
  Future<int> getMembershipCounter() async {
    final promise = getMembershipCounterJS();
    nbrMembers = int.parse(await promiseToFuture(promise));
    return nbrMembers;
  }

  //sum of Authorities
  Future<int> getAuthoritiesCounter() async {
    final promise = getAuthoritiesCounterJS();
    nbrAutorities = int.parse(await promiseToFuture(promise));
    return nbrAutorities;
  }

  Future<int> getCurrentSession() async {
    final currentSessionJS = await promiseToFuture(getCurrentSessionJS());
    currentSession = int.parse(
        currentSessionJS.toString() == '' ? '0' : currentSessionJS.toString());
    notifyListeners();
    return currentSession;
  }

  //get List of online Authorities pubkeys
  Future<Map<String, SmithData>> getSmiths() async {
    SmithStatus status;
    final onlineSmiths = await getListIndexes(getOnlineSmithsJS());
    final incomingAuthorities =
        await getListIndexes(getIncomingAuthoritiesJS());
    final outgoingAuthorities =
        await getListIndexes(getOutgoingAuthoritiesJS());

    for (final int smithIndex in onlineSmiths) {
      final address = await getIdentityAddress(smithIndex);

      if (incomingAuthorities.contains(smithIndex)) {
        status = SmithStatus.incoming;
      } else if (outgoingAuthorities.contains(smithIndex)) {
        status = SmithStatus.outgoing;
      } else if (onlineSmiths.contains(smithIndex)) {
        status = SmithStatus.online;
      } else {
        status = SmithStatus.offline;
      }

      listSmith.putIfAbsent(
          address,
          () => SmithData(
              address: address, idtyIndex: smithIndex, status: status));
    }

    // Get usernames
    final namesByAddresses =
        await indexer.namesByAddress(listSmith.keys.toList());
    listSmith.forEach((key, value) => value.username = namesByAddresses[key]);
    return listSmith;
  }

  Future<int> getAddressCerts(String address) async {
    final promise = getSmithCertsJS(address);
    final smithCertsJS = (await promiseToFuture(promise));
    final smithCert = int.parse(smithCertsJS[0].toString());

    return smithCert;
  }

  Future<int> getAddressSmithMembershipExpiration(int smithIndex) async {
    final promise2 = getSmithMembershipExpirationJS(smithIndex);
    final membershipExpirationJS = (await promiseToFuture(promise2));
    final membershipExpiration = (int.parse(
        membershipExpirationJS.toString() == ''
            ? '0'
            : membershipExpirationJS.toString()));
    return membershipExpiration;
  }

  Future<Map<String, SmithData>> getSmithsHardcoded() async {
    List<int> listIndexSmith = listIndexGenesis + listIndexRequestedSmith;
    final onlineSmiths = await getListIndexes(getOnlineSmithsJS());
    final incomingAuthorities =
        await getListIndexes(getIncomingAuthoritiesJS());
    final outgoingAuthorities =
        await getListIndexes(getOutgoingAuthoritiesJS());
    SmithStatus status;

    for (int smithIndex in listIndexSmith) {
      final address = await getIdentityAddress(smithIndex);
      final certs = await getAddressCerts(address);
      final expiration = await getAddressSmithMembershipExpiration(smithIndex);
      if (incomingAuthorities.contains(smithIndex)) {
        status = SmithStatus.incoming;
      } else if (outgoingAuthorities.contains(smithIndex)) {
        status = SmithStatus.outgoing;
      } else if (onlineSmiths.contains(smithIndex)) {
        status = SmithStatus.online;
      } else if (certs < 3) {
        status = SmithStatus.requested;
      } else if (expiration == 0) {
        status = SmithStatus.certified;
      } else {
        status = SmithStatus.offline;
      }
      //log.d(smithIndex);
      //log.d(expiration);
      listSmith.putIfAbsent(
          address,
          () => SmithData(
              address: address, idtyIndex: smithIndex, status: status));
    }
    final namesByAddresses =
        await indexer.namesByAddress(listSmith.keys.toList());
    listSmith.forEach((key, value) => value.username = namesByAddresses[key]);
    return listSmith;
  }

  //get list of indexes from JS function
  Future<List<int>> getListIndexes(promiseListIndexesJS) async {
    final List listIndexesJS = await promiseToFuture(promiseListIndexesJS);
    if (listIndexesJS.toString() == '[]') return [];
    final List<int> listIndexes = [];
    //TODO: remove this fucking toto trick needed for production mode...
    final toto = listIndexesJS
        .toString()
        .replaceAll('[', '')
        .replaceAll(']', '')
        .split(',');

    for (int i = 0; i < toto.length; i++) {
      listIndexes.add(int.parse(toto[i].toString()));
    }
    return listIndexes;
  }

  //get single index from pubkey
  Future<String> getIdentityAddress(int idtyIndex) async {
    final Map? idtyData = idtyIndex == 0
        ? null
        : json.decode(
            (await promiseToFuture(getIdentityDataJS(idtyIndex))).toString());
    return idtyData?['ownerKey'] ?? "";
  }

  Future<int> getMembershipExpiration(SmithData smith) async {
    final promise2 = getMembershipExpirationJS(smith.idtyIndex);
    final membershipExpirationJS = (await promiseToFuture(promise2));
    final membershipExpiration = (int.parse(
        membershipExpirationJS.toString() == ''
            ? '0'
            : membershipExpirationJS.toString()));
    return membershipExpiration;
  }

  Future<List<int>> getSmithCerts(SmithData smith) async {
    final promise = getSmithCertsJS(smith.address);
    final smithCertsJS = (await promiseToFuture(promise));
    final smithCerts = [
      int.parse(smithCertsJS[0].toString()),
      int.parse(smithCertsJS[1].toString())
    ];

    return smithCerts;
  }

  Future<int> getSmithMembershipExpiration(SmithData smith) async {
    final promise2 = getSmithMembershipExpirationJS(smith.idtyIndex);
    final smithMembershipExpirationJS = (await promiseToFuture(promise2));
    final smithMembershipExpiration = int.parse(
        smithMembershipExpirationJS.toString() == ''
            ? '0'
            : smithMembershipExpirationJS.toString());
    return smithMembershipExpiration;
  }

  Future<List<int>> getSessionExpiration(int idtyIndex) async {
    final promise = getSessionExpirationJS(idtyIndex);
    final sessionExpirationJS = (await promiseToFuture(promise));
    final sessionExpiration = [
      int.parse(sessionExpirationJS[0].toString()),
      int.parse(sessionExpirationJS[1].toString())
    ];
    return sessionExpiration;
  }

  void reload() {
    notifyListeners();
  }
}
