import 'package:flutter/material.dart';

class SearchProvider with ChangeNotifier {
  final searchController = TextEditingController();
  List searchResult = [];
  bool isLoading = false;

  void reload() {
    notifyListeners();
  }
}
