import 'dart:async';
import 'dart:convert';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/activity.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/balance.dart';
import 'package:gecko_web/widgets/loading.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:truncate/truncate.dart';

class HomeProvider with ChangeNotifier {
  String currentAddress = '';
  String currentName = '';
  final payAmount = TextEditingController();
  final mnemonicController = TextEditingController();

  // gql pagination
  int nRepositories = 20;
  List? parsedIndexerData;

  // router
  bool showCertsView = false;

  Future<String> getValidIndexerEndpoint() async {
    List listEndpoints = await rootBundle
        .loadString('config/indexer_endpoints.json')
        .then((jsonStr) => jsonDecode(jsonStr));
    // _listEndpoints.shuffle();

    // appVersion = await getAppVersion();

    return listEndpoints[0];

    // int i = 0;
    // // String _endpoint = '';
    // int statusCode = 0;

    // final client = HttpClient();
    // client.connectionTimeout = const Duration(milliseconds: 3000);

    // do {
    //   int listLenght = listEndpoints.length;
    //   if (i >= listLenght) {
    //     log.e('NO VALID INDEXER ENDPOINT FOUND');
    //     indexerEndpoint = '';
    //     break;
    //   }
    //   log.d('${i + 1}n indexer endpoint try: ${listEndpoints[i]}');

    //   if (i != 0) {
    //     await Future.delayed(const Duration(milliseconds: 300));
    //   }

    //   try {
    //     final request =
    //         await client.postUrl(Uri.parse('${listEndpoints[i]}/v1/graphql'));
    //     final response = await request.close();

    //     indexerEndpoint = listEndpoints[i];
    //     statusCode = response.statusCode;
    //     i++;
    //   } on TimeoutException catch (_) {
    //     log.e('This endpoint is timeout, next');
    //     statusCode = 50;
    //     i++;
    //     continue;
    //   } on SocketException catch (_) {
    //     log.e('This endpoint is a bad endpoint, next');
    //     statusCode = 70;
    //     i++;
    //     continue;
    //   } on Exception {
    //     log.e('Unknown error');
    //     statusCode = 60;
    //     i++;
    //     continue;
    //   }
    // } while (statusCode != 200);

    // log.i('INDEXER: $indexerEndpoint');
    // return indexerEndpoint;
  }

  Future<String> getAppVersion() async {
    String version;
    String buildNumber;
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    version = packageInfo.version;
    buildNumber = packageInfo.buildNumber;

    notifyListeners();
    return 'v$version+$buildNumber';
  }

  List<Widget> parseLatestTransactions(
      BuildContext context, List resultQ, Map pageInfo) {
    List<Widget> transactionsList = [];

    for (final transactionNode in resultQ) {
      final transaction = transactionNode['node'];
      final String issuerAddress = transaction['issuer_pubkey'];
      final String issuerName =
          transaction['issuer']['identity']?['name'] ?? '';
      final String receiverAddress = transaction['receiver_pubkey'];
      final String receiverName =
          transaction['receiver']['identity']?['name'] ?? '';
      final int amount = transaction['amount'];
      final DateTime date = DateTime.parse(transaction['created_at']).toLocal();
      transactionsList.add(rowTransaction(context, issuerAddress, issuerName,
          receiverAddress, receiverName, amount, date));
    }

    return transactionsList;
  }

  Widget rowTransaction(
      BuildContext context,
      String issuerAddress,
      String issuerName,
      String receiverAddress,
      String receiverName,
      int amount,
      DateTime date) {
    final double humainAmount = amount / 100;
    final String year = truncate("${date.year}", 2,
        omission: "", position: TruncatePosition.start);
    final month = monthsInYear[date.month];
    final String humainDate =
        "${date.day} ${month!.replaceRange(3, month.length, '.')} $year";
    final String hour = "${date.hour}".padLeft(2, '0');
    final String minute = "${date.minute}".padLeft(2, '0');
    final String humainHour = "$hour:$minute";

    return SizedBox(
      height: 45,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 80,
            child: Column(
              children: [
                Text(humainDate,
                    style: const TextStyle(fontWeight: FontWeight.w500)),
                const SizedBox(height: 2),
                Text(humainHour,
                    style: TextStyle(
                        color: Theme.of(context).textTheme.titleLarge!.color)),
              ],
            ),
          ),
          Builder(
            builder: (context) => SizedBox(
              width: 120,
              child: InkWell(
                onTap: () {
                  currentAddress = issuerAddress;
                  currentName = issuerName;

                  notifyListeners();
                  if (screenWidth < 1130) {
                    Scaffold.of(context).openEndDrawer();
                  }
                },
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        getShortPubkey(issuerAddress),
                        style: const TextStyle(fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(height: 2),
                      Text(
                        issuerName,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color:
                                Theme.of(context).textTheme.titleLarge!.color),
                      ),
                    ]),
              ),
            ),
          ),
          const Icon(Icons.arrow_forward, size: 18),
          const SizedBox(width: 0),
          Builder(
            builder: (context) => SizedBox(
              width: 120,
              child: InkWell(
                onTap: () {
                  currentAddress = receiverAddress;
                  currentName = receiverName;
                  notifyListeners();
                  if (screenWidth < 1130) {
                    Scaffold.of(context).openEndDrawer();
                  }
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      getShortPubkey(receiverAddress),
                      style: const TextStyle(fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(height: 2),
                    Text(
                      receiverName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Theme.of(context).textTheme.titleLarge!.color),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 100,
            child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              Text('$humainAmount $currencyName',
                  style: TextStyle(
                      color: Theme.of(context).textTheme.titleLarge!.color))
            ]),
          )
        ],
      ),
    );
  }

  List<Widget> parseLatestIdentity(BuildContext context, List resultQ) {
    List<Widget> transactionsList = [];

    for (final transactionNode in resultQ) {
      final transaction = transactionNode['node'];
      final String address = transaction['pubkey'];
      final String name = transaction['name'] ?? '';
      final DateTime date =
          DateTime.parse(transaction['confirmed_at']).toLocal();
      transactionsList.add(rowIdentity(context, address, name, date));
    }

    return transactionsList;
  }

  Widget rowIdentity(
      BuildContext context, String address, String name, DateTime date) {
    // SearchProvider search = Provider.of<SearchProvider>(context, listen: false);
    final String year = truncate("${date.year}", 2,
        omission: "", position: TruncatePosition.start);
    final month = monthsInYear[date.month];
    final String humainDate =
        "${date.day} ${month!.replaceRange(3, month.length, '.')} $year";

    // final String hour = "${date.hour}".padLeft(2, '0');
    // final String minute = "${date.minute}".padLeft(2, '0');
    // final String humainHour = "$hour:$minute";

    return SizedBox(
      height: 45,
      child: Builder(
        builder: (context) => InkWell(
          onTap: () {
            currentAddress = address;
            currentName = name;
            notifyListeners();
            if (screenWidth < 1130) {
              Scaffold.of(context).openEndDrawer();
            }
          },
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Column(
              children: [
                SizedBox(
                  width: 100,
                  child: Text(humainDate,
                      style: const TextStyle(fontWeight: FontWeight.w500)),
                ),
                // Text(
                //   humainHour,
                //   style: TextStyle(
                //       fontWeight: FontWeight.w100,
                //       color: Colors.grey[400],
                //       fontSize: 14),
                // ),
              ],
            ),
            SizedBox(
              width: 150,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          fontWeight: FontWeight.w500, fontSize: 17),
                    ),
                    Text(
                      getShortPubkey(address),
                      style: TextStyle(
                          color: Theme.of(context).textTheme.titleLarge!.color),
                    ),
                    const SizedBox(height: 2),
                  ]),
            ),
            const SizedBox(width: 0),
            SizedBox(
              width: 100,
              child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                Balance(
                    address: address,
                    size: 14,
                    color: Theme.of(context).textTheme.titleLarge!.color!),
              ]),
            )
          ]),
        ),
      ),
    );
  }

  InlineSpan makeTooltip(BuildContext context, String address, String name) {
    return WidgetSpan(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            key: const Key('copyPubkey'),
            onTap: () {
              Clipboard.setData(ClipboardData(text: address));
              snackCopyKey(context, address);
            },
            child: Text(
              getShortPubkey(address),
              style: const TextStyle(fontWeight: FontWeight.w600),
            ),
          ),
          const SizedBox(height: 2),
          Text(
            name,
            style:
                TextStyle(color: Theme.of(context).textTheme.titleLarge!.color),
          ),
        ],
      ),
    );
  }

  String getShortPubkey(String pubkey) {
    String pubkeyShort = truncate(pubkey, 7,
            omission: String.fromCharCode(0x2026),
            position: TruncatePosition.end) +
        truncate(pubkey, 6, omission: "", position: TruncatePosition.start);
    return pubkeyShort;
  }

  snackCopyKey(BuildContext context, String address) {
    Clipboard.setData(ClipboardData(text: address));

    final snackBar = SnackBar(
        padding: const EdgeInsets.all(20),
        content: Text("thisAddressHasBeenCopiedToClipboard".tr(),
            style: const TextStyle(fontSize: 16)),
        duration: const Duration(seconds: 2));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  bool isAddress(String address) {
    final RegExp regExp = RegExp(
      r'^[a-zA-Z0-9]+$',
      caseSensitive: false,
      multiLine: false,
    );

    if (regExp.hasMatch(address) == true &&
        address.length > 45 &&
        address.length < 52) {
      return true;
    } else {
      return false;
    }
  }

  void reload() {
    notifyListeners();
  }

  Map<String, List<int>> certsCache = {};

  Widget getCerts(BuildContext context, String address, double size,
      [Color color = Colors.black]) {
    final polka = Provider.of<PolkadotProvider>(context, listen: false);

    return Column(children: <Widget>[
      Consumer<PolkadotSubscribBlocksProvider>(
          builder: (context, polkaBlock, _) {
        return FutureBuilder(
            future: polka.getCerts(address),
            builder: (BuildContext context, AsyncSnapshot<List<int>> certs) {
              if (certs.hasError) {
                return const SizedBox();
              }
              if (certs.connectionState != ConnectionState.done) {
                if (certsCache[address] != null &&
                    (certsCache[address]![0] != 0 ||
                        certsCache[address]![1] != 0)) {
                  if (certs.data == null) {
                    return const SizedBox();
                  }

                  Row(
                    children: [
                      Image.asset('assets/medal.png', height: size),
                      const SizedBox(width: 1),
                      Text(certsCache[address]?[0].toString() ?? '0',
                          style: TextStyle(fontSize: size, color: color)),
                      const SizedBox(width: 2),
                      Text(
                        "(${certsCache[address]?[1].toString() ?? '0'})",
                        style: TextStyle(fontSize: size * 0.6, color: color),
                      )
                    ],
                  );
                } else {
                  return const Loading();
                }
              }

              certsCache[address] = certs.data ?? [];

              return certsCache[address] != null &&
                      (certsCache[address]![0] != 0 ||
                          certsCache[address]![1] != 0)
                  ? Row(
                      children: [
                        Image.asset('assets/medal.png', height: size),
                        const SizedBox(width: 1),
                        Text(certsCache[address]?[0].toString() ?? '0',
                            style: TextStyle(fontSize: size, color: color)),
                        const SizedBox(width: 2),
                        Text(
                          "(${certsCache[address]?[1].toString() ?? '0'})",
                          style: TextStyle(fontSize: size * 0.6, color: color),
                        )
                      ],
                    )
                  : const Text('');
            });
      }),
    ]);
  }

  FetchMoreOptions mergeQueryResult(
      BuildContext context, String req, QueryResult result,
      {String? address}) {
    final activityProvider =
        Provider.of<ActivityProvider>(context, listen: false);

    List<dynamic> resultData = result.data![req]['edges'];

    final Map pageInfo = result.data![req]['pageInfo'];
    final String fetchMoreCursor = pageInfo['endCursor'];

    final opts = FetchMoreOptions(
      variables: {'cursor': fetchMoreCursor, 'number': nRepositories},
      updateQuery: (previousResultData, fetchMoreResultData) {
        final List<dynamic> repos = [
          ...previousResultData![req]['edges'] as List<dynamic>,
          ...fetchMoreResultData![req]['edges'] as List<dynamic>
        ];

        fetchMoreResultData[req]['edges'] = repos;

        return fetchMoreResultData;
      },
    );

    log.d("###### DEBUG pagination $req - Cursor: $fetchMoreCursor ######");
    switch (req) {
      case 'transaction_connection':
        parsedIndexerData = address == null
            ? parseLatestTransactions(context, resultData, pageInfo)
            : activityProvider.parseHistory(resultData, address);
        break;
      case 'identity_connection':
        parsedIndexerData = parseLatestIdentity(context, resultData);
        break;
    }
    return opts;
  }
}
