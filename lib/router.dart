import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/screens/certifications.dart';
import 'package:gecko_web/screens/home.dart';
import 'package:gecko_web/screens/smiths.dart';

part 'router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  final List<AutoRoute> routes = [
    AutoRoute(path: '/', page: MyHomeRoute.page),
    AutoRoute(path: '/smiths', page: SmithsRoute.page),
    AutoRoute(path: '/profile/certifications', page: CertificationsRoute.page),
  ];
}
