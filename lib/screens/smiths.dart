import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/providers/polkadot_subscribe_sessions.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/smiths/grid_widget.dart';
import 'package:gecko_web/widgets/smiths/loading.dart';
import 'package:gecko_web/widgets/smiths/smith_details.dart';
import 'package:gecko_web/widgets/smiths/smith_tile.dart';
import 'package:provider/provider.dart';

@RoutePage()
class SmithsPage extends StatelessWidget {
  const SmithsPage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final smithsP = Provider.of<SmithsProvider>(context, listen: false);

    return MaterialApp(
      home: SelectionArea(
        child: Scaffold(
          appBar: AppBar(
            actions: const [SizedBox()],
            leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.grey[900]),
                onPressed: () {
                  context.router.canPop()
                      ? context.router.pop()
                      : context.router.pushNamed('/');
                }),
            title: Consumer3<SmithsProvider, PolkadotSubscribBlocksProvider,
                PolkadotSubscribSessionsProvider>(
              builder: (context, smithsP, polkaSubBlock, polkaSubSession, _) {
                return Center(
                  child: !smithsP.isSmithStartupLoaded
                      ? Text(
                          "loading".tr(),
                          style: const TextStyle(
                            fontSize: 20,
                            decoration: TextDecoration.none,
                            color: Colors.black,
                          ),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              '''${'currentTimeFromBlocks'.tr()}: ${(DateFormat('d MMM yyyy HH:mm:ss').format(
                                blocNumberToDate(polkaSubBlock.blockNumber),
                              )).toString()}\n${"currentBlock".tr()}: ${polkaSubBlock.blockNumber}''',
                              style: const TextStyle(
                                fontSize: 20,
                                decoration: TextDecoration.none,
                                color: Colors.black,
                              ),
                            ),
                            Text(
                              '''${'currentTimeFromSessions'.tr()}: ${(DateFormat('d MMM yyyy HH:mm:ss').format(
                                sessionNumberToDate(
                                    polkaSubSession.sessionNumber),
                              )).toString()}\n${"currentSession".tr()}: ${polkaSubSession.sessionNumber}''',
                              style: const TextStyle(
                                fontSize: 20,
                                decoration: TextDecoration.none,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                );
              },
            ),
            backgroundColor: yellowC,
          ),
          endDrawer: Drawer(
              width: 500,
              child: SmithDetails(
                key: key,
              )),
          body: FutureBuilder(
            // Here we initialize every async function we need before start the app
            future: smithsP.asyncStartup(context),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              if (snapshot.connectionState != ConnectionState.done) {
                return LoadingWidget(loadingText: 'Loading'.tr());
              }

              screenWidth = MediaQuery.of(context).size.width;
              screenHeight = MediaQuery.of(context).size.width;

              // fill a list en SmithTiles widgets
              final List<SmithTile> smithsTilesOnline = [];
              final List<SmithTile> smithsTilesOffline = [];
              final List<SmithTile> smithsTilesRequested = [];
              final List<SmithTile> smithsTilesCertified = [];

//               log.i('''
// currentSession: ${smithsP.currentSession}
// smithsP.listSmith.values.first.address: ${smithsP.listSmith.values.first.address}
// ''');

              for (final smith in smithsP.listSmith.values) {
                if (smith.status == SmithStatus.online ||
                    smith.status == SmithStatus.outgoing) {
                  smithsTilesOnline.add(
                    SmithTile(smith: smith),
                  );
                } else if (smith.status == SmithStatus.offline ||
                    smith.status == SmithStatus.incoming) {
                  smithsTilesOffline.add(
                    SmithTile(smith: smith),
                  );
                } else if (smith.status == SmithStatus.requested) {
                  smithsTilesRequested.add(
                    SmithTile(smith: smith),
                  );
                } else if (smith.status == SmithStatus.certified) {
                  smithsTilesCertified.add(
                    SmithTile(smith: smith),
                  );
                }
              }

              int nTule;
              if (screenWidth >= 1500) {
                nTule = 8;
              } else if (screenWidth >= 1200) {
                nTule = 6;
              } else {
                nTule = 4;
              }

              // Build the list of SmithTiles
              return SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(height: 15),
                    Consumer<SmithsProvider>(builder: (context, smithsP, _) {
                      return Column(children: [
                        Text(
                            '${"numberAutorities".tr()}: ${smithsP.nbrAutorities}',
                            style: const TextStyle(fontSize: 19)),
                        const SizedBox(height: 5),
                        Text(
                            '${"numberSmithMembership".tr()}: ${smithsP.nbrMembers}',
                            style: const TextStyle(fontSize: 19)),
                        const SizedBox(height: 10),
                        Row(
                          children: [
                            Text('${"online".tr()}:',
                                style: const TextStyle(fontSize: 19))
                          ],
                        ),
                        GridWidget(
                            nTule: nTule, smithsTiles: smithsTilesOnline),
                        const SizedBox(height: 10),
                        Row(
                          children: [
                            Text("${"offline".tr()}:",
                                style: const TextStyle(fontSize: 19))
                          ],
                        ),
                        GridWidget(
                            nTule: nTule, smithsTiles: smithsTilesOffline),
                        const SizedBox(height: 10),
                        Row(
                          children: [
                            Text("${"certified".tr()}:",
                                style: const TextStyle(fontSize: 19))
                          ],
                        ),
                        GridWidget(
                            nTule: nTule, smithsTiles: smithsTilesCertified),
                        Row(
                          children: [
                            Text("${"requested".tr()}:",
                                style: const TextStyle(fontSize: 19))
                          ],
                        ),
                        GridWidget(
                            nTule: nTule, smithsTiles: smithsTilesRequested),
                      ]);
                    }),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
