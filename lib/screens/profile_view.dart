import 'package:flutter/material.dart';
import 'package:gecko_web/widgets/widgets_activity.dart';
import 'package:gecko_web/widgets/widgets_profiles.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: const <Widget>[
      HeaderProfileView(),
      HistoryQuery(),
      // PayPopup()
    ]);
  }
}
