// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    CertificationsRoute.name: (routeData) {
      final args = routeData.argsAs<CertificationsRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: CertificationsScreen(
          key: args.key,
          address: args.address,
          username: args.username,
        ),
      );
    },
    MyHomeRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MyHomePage(),
      );
    },
    SmithsRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SmithsPage(),
      );
    },
  };
}

/// generated route for
/// [CertificationsScreen]
class CertificationsRoute extends PageRouteInfo<CertificationsRouteArgs> {
  CertificationsRoute({
    Key? key,
    required String address,
    required String username,
    List<PageRouteInfo>? children,
  }) : super(
          CertificationsRoute.name,
          args: CertificationsRouteArgs(
            key: key,
            address: address,
            username: username,
          ),
          initialChildren: children,
        );

  static const String name = 'CertificationsRoute';

  static const PageInfo<CertificationsRouteArgs> page =
      PageInfo<CertificationsRouteArgs>(name);
}

class CertificationsRouteArgs {
  const CertificationsRouteArgs({
    this.key,
    required this.address,
    required this.username,
  });

  final Key? key;

  final String address;

  final String username;

  @override
  String toString() {
    return 'CertificationsRouteArgs{key: $key, address: $address, username: $username}';
  }
}

/// generated route for
/// [MyHomePage]
class MyHomeRoute extends PageRouteInfo<void> {
  const MyHomeRoute({List<PageRouteInfo>? children})
      : super(
          MyHomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'MyHomeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SmithsPage]
class SmithsRoute extends PageRouteInfo<void> {
  const SmithsRoute({List<PageRouteInfo>? children})
      : super(
          SmithsRoute.name,
          initialChildren: children,
        );

  static const String name = 'SmithsRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
