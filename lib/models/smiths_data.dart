class SmithData {
  String address;
  int idtyIndex;
  String? username;
  SmithStatus? status;
  List<int>? memberCerts;
  List<int>? smithCerts;

  SmithData(
      {required this.address,
      required this.idtyIndex,
      this.username,
      this.status,
      this.memberCerts,
      this.smithCerts});
}

enum SmithStatus {
  online,
  offline,
  incoming,
  outgoing,
  certified,
  requested,
}
