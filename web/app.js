
async function getIdentityIndexJS(address) {
    return await api.query.identity.identityIndexOf(address)
}

async function getIdentityDataJS(idtyIdenx) {
    return await api.query.identity.identities(idtyIdenx)
}

async function getCurrentUdIndexJS() {
    return await api.query.universalDividend.currentUdIndex()
}

async function getPastReevalsJS() {
    const result = (await api.query.universalDividend.pastReevals()).toString()
    const obj = JSON.parse(result)
    return obj
}

async function connectNodeJS(endpoint) {
    // const { pair, json } = keyring.addUri('smart joy blossom stomach champion fun diary relief gossip hospital logic bike', 'myStr0ngP@ssworD', { name: 'mnemonic acc' });
    console.log("Connection to endpoint " + endpoint);
    const connected = await settings.connect([endpoint]);
    return connected
}

async function getBalanceJS(address) {
    const balanceR = await api.query.system.account(address);
    return balanceR['data'];
}

async function getIdentitiesNumberJS() {
    return (await api.query.identity.counterForIdentities()).toString();
}

async function getCertsJS(address) {
    const idtyIndex = await api.query.identity.identityIndexOf(address)
    const _certsReceiver = (await api.query.cert.storageIdtyCertMeta(idtyIndex.toString())).toString()
    const obj = JSON.parse(_certsReceiver);

    return [+obj['receivedCount'], +obj['issuedCount']];
}

async function subscribeNewBlocsJS(callback) {
    await api.rpc.chain.subscribeNewHeads((header) => callback(header))
}

////////////////////////
//////// Account ///////
////////////////////////

// import account

async function importAccountJS() {
    const { pair, json } = keyring.addUri(mnemonic, 'myStr0ngP@ssworD', { name: 'mnemonic acc' });
}

////////////////////////
//////// Smiths ////////
////////////////////////

//counter of membership
async function getMembershipCounterJS() {
    return (await api.query.smithsMembership.counterForMembership()).toString();
}

//counter of membership
async function getAuthoritiesCounterJS() {
    return (await api.query.authorityMembers.authoritiesCounter()).toString();
}

//indexes of online Authorities (will be parsed List int)
async function getOnlineSmithsJS() {
    return await api.query.authorityMembers.onlineAuthorities()
}

//indexes of incoming Authorities (will be parsed List int)
async function getIncomingAuthoritiesJS() {
    return await api.query.authorityMembers.incomingAuthorities();
}

//indexes of outgoing Authorities (will be parsed List int)
async function getOutgoingAuthoritiesJS() {
    return await api.query.authorityMembers.outgoingAuthorities();
}

//Membership Expiration (will be parsed int)
async function getMembershipExpirationJS(idtyIdenx) {
    const membershipExpiration = (await api.query.membership.membership(idtyIdenx.toString())).toString();
    obj = JSON.parse(membershipExpiration);
    return obj['expireOn']
}

async function getSessionExpirationJS(idtyIdenx) {
    const sessionExpiration = (await api.query.authorityMembers.members(idtyIdenx.toString())).toString();
    obj = JSON.parse(sessionExpiration);
    return [+obj['expireOnSession'], +obj['mustRotateKeysBefore']];
}

//counter of smith certs received and issued (will be parsed List int)
async function getSmithCertsJS(address) {
    const idtyIndex = await api.query.identity.identityIndexOf(address)
    const _smithCertsReceiver = (await api.query.smithsCert.storageIdtyCertMeta(idtyIndex.toString())).toString();
    const obj = JSON.parse(_smithCertsReceiver);

    return [+obj['receivedCount'], +obj['issuedCount']];
}

//Smith Membership Expiration (will be parsed int)
async function getSmithMembershipExpirationJS(idtyIdenx) {
    const smithMembershipExpiration = (await api.query.smithsMembership.membership(idtyIdenx.toString())).toString();
    obj = JSON.parse(smithMembershipExpiration);
    //if(obj.hasOwnProperty('expireOn'))
    return obj['expireOn']
}

// We use app JS object directly to test this way
async function getCurrentSessionJS() {
    return await api.query.session.currentIndex()
}

///////////////////////////////
//////// SUBSCRIPTIONS ////////
///////////////////////////////

async function subscribeNewSessionsJS(callback) {
    return await api.query.session.currentIndex((sessionNum) => callback(sessionNum))
}